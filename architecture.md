# Scenario Implementation 

# Scenario Architecture Schema

```mermaid
flowchart
    subgraph Gaia-X AISBL
        subgraph compliance.gaia-x.eu
            GX-TF[Gaia-X TrustFramework]
        end
        subgraph registry.gaia-x.eu
            GX-Reg[Gaia-X Registry]
        end
        GX-TF<-->GX-Reg
    end

    subgraph Identity Provider?
        id_VP(Id VP) <--in--> GX-Reg
        id_service[Id Service] <-- sign --> id_VP
    end

    subgraph Participant *Consumer*
            SD_C[Consumer SelfDescriptions]
            Policy_C[Consumer Policy]
            VP_C[Consumer VPs] <--> id_service
    end

    subgraph Participant *Provider*
            SD_P[Provider SelfDescriptions]
            Policy_P[Provider Policy]
            VP_P[Provider VPs] <--> id_service
    end

    subgraph Participant *Federator*
        SD_F[Federator SelfDescription]
        F-catalogue[Federation Catalogue]
        FT-catalogue[Trust Service Catalogue]
        VP_F[Federator VPs] <--> id_service
        subgraph Compliance Service
            F-Portal[Federation Portal] <--> ClaimsValidator[Claims Validator] <--> VC[VC Issuer]
            F-Portal[Federation Portal] <--> Label[Label] <--> VC[VC Issuer] <--> VP_F
        end
        subgraph TBC
            F-TF[Federation TrustFramework]
            F-SD_schema[Federation Extended Schema]
            F-Reg[Federation Registry]
        end
        F-TF<-->F-Reg
    end

    subgraph Service Offering
            Policy_Check[Policy Rules Checker] <-->  Id_Check[Identity Check]
    end
    
    Policy_P <--> Policy_Check <--> Policy_C
    SD_C <--> GX-TF
    SD_P <--> GX-TF
    SD_F <--> GX-TF
    F-TF<-->GX-TF
    F-Reg<-->GX-Reg
    Id_Check<-->GX-Reg

```
